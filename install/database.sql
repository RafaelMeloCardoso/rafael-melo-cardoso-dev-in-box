/**
 * Estrutura padrão da tabela de tarefas
 */
CREATE TABLE tarefas (
  id         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  titulo     VARCHAR(50)  NOT NULL,
  descricao  VARCHAR(255) NOT NULL,
  prioridade SMALLINT     DEFAULT 1,
  criacao    DATETIME     DEFAULT now(),
  edicao     DATETIME     DEFAULT now()
);

/**
 * Exemplos já inseridos
 */
INSERT INTO tarefas(titulo, descricao, prioridade)
VALUES
  ('Desenvolver API', 'Desenvolver API REST usando CakePHP. PS: Compativel com 5.6', 1),
  ('Desenvolver Frontend', 'Desenvolver Frontend responsiva para gerenciamento das tarefas', 2),
  ('Desenvolver Drag and Drop', 'Utilizar Interface drag and drop. PS: Tomar cuidado com mobile.', 3);
