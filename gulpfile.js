/**
 * Created by Rafael Melo Cardoso <xinhus@gmail.com> on 10/30/16 14:28 PM
 */


var gulp;
var sass;
var notify;
var concat;
gulp = require("gulp");
sass = require("gulp-sass");
concat = require("gulp-concat");
notify = require("gulp-notify");

/**
 * Configuraçõe de quais pastas deve conferir
 * @type {{pastaSass: string, pastaCss: string, pastaJs: string, pastaDestinoJs: string}}
 */
var config = {
    pastaSass : "./app/webroot/src/scss",
    pastaCss : "./app/webroot/css",
    pastaJs : "./app/webroot/src/js",
    pastaDestinoJs : "./app/webroot/js"
};

/**
 * Tarefa que compila o sass.
 * Tambem ja importa o sass do bootstrap.
 */
gulp.task("compilar:sass", function() {
    return gulp
        .src(config.pastaSass + "/default.scss")
        .pipe(
            sass({
                outputStyle: "compressed",
                includePaths: [
                    config.pastaSass,
                    "./bower_components/bootstrap-sass/assets/stylesheets"
                ]
            })
            .on("error", notify.onError(function (error) {
                return "Erro ao compilar o Sass: " + error.message;
            }))
        )
        .pipe(gulp.dest(config.pastaCss));
});

/**
 * Tarefa que compila os JS e importa os nossos js
 */
gulp.task("compilar:js", function() {
    return gulp
        .src([
            "./bower_components/jquery/dist/jquery.min.js",
            "./bower_components/bootstrap-sass/assets/bootstrap/jquery.min.js",
            "./bower_components/jquery-ui/jquery-ui.js",
            config.pastaJs + "/GerenciadorTarefas.js",
            config.pastaJs + "/default.js",
        ])
        .pipe(concat("default.js"))
        .pipe(gulp.dest(config.pastaDestinoJs))
});



/**
 * Watch para compilar o sass e js
 */
gulp.task("watch", function() {
    gulp.watch(config.pastaSass + "/**/*.scss", ["compilar:sass"]);
    gulp.watch(config.pastaJs + "/**/*.js", ["compilar:js"]);
});


/**
 * Tarefa padrao
 */
gulp.task("default", ["compilar:sass", "compilar:js"]);
