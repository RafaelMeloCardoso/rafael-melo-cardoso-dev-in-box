<?php

/**
 * Author: Rafael Melo Cardoso <xinhus@gmail.com>
 * Date: 10/30/16
 * Time: 1:30 PM
 */

/**
 * Class Tarefas responsável por gerenciar as tarefas juntamente ao banco
 */
class Tarefa extends AppModel
{
    /**
     * Realiza a inserção de tarefas validando se existe título e descrição.
     *
     * @param array $parametros
     * @return bool | array
     * @throws Exception
     */
    public function inserirTarefa(array $parametros)
    {
        if (!isset($parametros['titulo']) || empty($parametros['titulo'])) {
            throw new Exception("O campo título e obrigatório");
        }

        if (!isset($parametros['descricao']) || empty($parametros['descricao'])) {
            throw new Exception("O campo descrição e obrigatório");
        }


        $informacoes = array(
            'titulo'    => $parametros['titulo'],
            'descricao' => $parametros['descricao'],
            'prioridade'=> (isset($parametros['prioridade']) ? $parametros['prioridade'] : 1 )
        );

        $this->create();
        $sucesso = $this->save($informacoes);

        if ($sucesso) {
            return $sucesso;
        } else {
            throw new Exception(implode("\n", $this->validationErrors));
        }
    }

    /**
     * Realiza a edição de uma tarefa.
     * Mantém a data de edição atualizada
     * Também garantimos que so ira editar o necessario.
     *
     * @param $id
     * @param array $parametros
     * @return mixed
     * @throws Exception
     */
    public function editarTarefa($id, array $parametros)
    {
        if (!$this->exists($id)) {
            throw new Exception("Regitro não encontrado");
        }

        $informacoes = array();

        if (isset($parametros['titulo'])) {
            $informacoes['titulo'] = $parametros['titulo'];
            if (empty($parametros['titulo'])) {
                throw new Exception("O campo título e obrigatório");
            }
        }

        if (isset($parametros['descricao'])) {
            $informacoes['descricao'] = $parametros['descricao'];
            if (empty($parametros['descricao'])) {
                throw new Exception("O campo descrição e obrigatório");
            }
        }

        if (isset($parametros['prioridade'])) {
            $informacoes['prioridade'] = $parametros['prioridade'];
        }

        $db = $this->getDataSource();
        $informacoes['edicao'] = $db->expression('NOW()');


        $this->id = $id;


        $sucesso = $this->save($informacoes);

        if ($sucesso) {
            return $sucesso;
        } else {
            throw new Exception(implode("\n", $this->validationErrors));
        }

    }

}