/**
 * Created by Rafael Melo Cardoso <xinhus@gmail.com> on 10/30/16 18:30 PM
 */

//Neste caso nao precisaria passar os parametros pois sao os mesmo que o default. Mas para exemplificar...
$('body').gerenciadorTarefas({
    listagem: ".lista-tarefas",
    formulario: ".form-tarefas"
});
