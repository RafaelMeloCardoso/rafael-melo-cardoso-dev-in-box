/**
 * Created by Rafael Melo Cardoso <xinhus@gmail.com> on 10/30/16 14:30 PM
 */

(function($) {

    //Padrao de frontend para tarefas
    var tarefaIndividual = $('<li class="tarefas"><div class="postit"><span class="titulo"></span><span class="descricao"></span><i class="material-icons btn-editar">mode_edit</i><i class="material-icons btn-remover">delete</i></div></li>')

    var api = {

        //Responsavel pela listagem de tarefas da nossa api
        listagem: function () {
            $.get({
                url : 'tarefas.json',
                dataType: 'json',
                success: function(data) {
                    for (i = 0; i < data.Tarefas.length ; i++) {
                        frontend.adicionarTarefaListagem(data.Tarefas[i], false);
                    }
                    frontend.atualizarAlturas(false);
                }
            });

        },

        //Reponsavel por adicionar itens
        adicionar: function () {
            $.post({
                url : 'tarefas.json',
                dataType: 'json',
                data: frontend.formulario.serialize(),
                success: function(data) {
                    if (data.sucesso) {
                        frontend.adicionarTarefaListagem(data, true);
                        frontend.formulario[0].reset();
                        frontend.atualizarAlturas(false);
                        frontend.atualizarPrioridades();
                    } else {
                        alert(data.mensagem);
                    }
                }
            });
            return false;
        },

        //Responsavel por excluir tarefas.
        excluir: function(id, elemento) {
            $.ajax({
                url: 'tarefas/' + id + '.json',
                type: 'DELETE',
                success: function(result) {
                    frontend.removerTarefa(elemento);
                },
                error: function (result) {
                    if (result.status = '404') {
                        alert("Tarefa nao encontrada!");
                    } else {
                        alert(result.statusText);
                    }

                }
            });
        },

        // Reponsavel por atualizar as prioridades das tarefas
        atualizarPrioridade: function(id, prioridade, elemento) {

            //Apenas chamo o metodo de atualizaçao de tarefa.
            this._atualizar(id, {prioridade:prioridade}, elemento, false);
        },

        //Responsavel por atualizar a tarefa Completa
        atualizarTarefa: function(id, titulo, descricao, elemento) {

            //Apenas chamo o metodo de atualizaçao de tarefa.
            this._atualizar(
                id,
                {
                    titulo:titulo,
                    descricao:descricao
                },
                elemento,
                true
            );
        },

        //Metodo responsavel por realizar a chamada para atualizar uma tarefa.
        _atualizar: function(id, informacoes, elemento, renderizar) {
            $.post({
                url : 'tarefas/' + id + '.json',
                dataType: 'json',
                data: informacoes,
                success: function(data) {
                    if (data.sucesso) {
                        if (data.Tarefa.prioridade !== undefined) {
                            elemento.data('prioridade', data.Tarefa.prioridade);
                        }

                        if (renderizar) {
                            elemento.find('.titulo').html(data.Tarefa.titulo);
                            elemento.find('.descricao').html(data.Tarefa.descricao);
                            elemento.find('.btn-editar').show();
                            elemento.find('.btn-salvar').remove();
                            frontend.atualizarAlturas(true);
                        }
                    } else {
                        alert(data.mensagem);
                    }
                },
                error: function (result) {
                    if (result.status = '404') {
                        alert("Tarefa nao encontrada!");
                    } else {
                        alert(result.statusText);
                    }

                }
            });

        }



    };



    var frontend = {

        //Armazena se ja foi inicializado nosso componente.
        inicializado: false,

        //Elemento da nossa listagem de tarefas
        listagem:null,

        //Elemento do formulario.
        formulario:null,


        // Responsavel por adicionar tarefas a nossa listagem
        adicionarTarefaListagem: function (data, inserirComoPrimeiro) {
            if (this.inicializado) {
                var _tarefa = tarefaIndividual.clone();

                _tarefa.find('.titulo').html(data.Tarefa.titulo);
                _tarefa.find('.descricao').html(data.Tarefa.descricao);

                _tarefa.find('.btn-remover').on('click', function(){
                    deletar = confirm("Voce realmente deseja excluir?\nImpossivel reverter.");
                    if (deletar) {
                        api.excluir(data.Tarefa.id, _tarefa);
                    }
                });

                _tarefa.find('.btn-editar').on('click', function(){
                    frontend.editarTarefa(_tarefa);
                });

                var prioridade = this.listagem.find('.tarefas').length + 1;

                _tarefa.data('prioridade', prioridade);
                _tarefa.data('id', data.Tarefa.id);

                if (inserirComoPrimeiro) {
                    _tarefa.prependTo(this.listagem);
                } else {
                    _tarefa.appendTo(this.listagem);
                }

            }
        },

        //Responsavel por remover Tarefas de nossa listagem
        removerTarefa: function(elemento) {
            $(elemento).remove();
            this.atualizarAlturas(false);
            this.atualizarPrioridades();
        },

        //Organiza alturar. Como os frontend tem tamanhos diferentes nos atualizar suas alturas
        atualizarAlturas: function(reset) {
            var altura = 0;

            if (reset) {
                this.listagem.find('.tarefas .postit').each(function () {
                    $(this).height("auto");
                });
            }
            this.listagem.find('.tarefas .postit').each(function () {
                var _elemento = $(this);
                altura = altura > _elemento.height() ? altura : _elemento.height();
            });

            this.listagem.find('.tarefas .postit').each(function () {
                $(this).height(altura);
            });
        },

        //verifica quais tarefas tiveram suas prioridades alteradas e chama a API
        atualizarPrioridades: function() {
            this.listagem.find('.tarefas').each(function () {
                var _elemento = $(this);
                if (_elemento.data('prioridade') != (_elemento.index()+1)) {
                    api.atualizarPrioridade(_elemento.data('id') , (_elemento.index()+1), _elemento);
                }
            });
        },

        //Editar Tarefa
        editarTarefa: function(elementoTarefa) {
            var postit = elementoTarefa.find('.postit');
            var spanTitulo = elementoTarefa.find('.titulo');
            var spanDescricao = elementoTarefa.find('.descricao');

            var btnEditar = elementoTarefa.find('.btn-editar');

            var inputTitulo = $('<input type="text" class="form-control" maxlength="50"/>');
            var inputDescricao = $('<input type="text" class="form-control" maxlength="255"/>');
            var btnSalvar = $('<button type="button" class="btn btn-success btn-salvar">Salvar</button>');


            inputDescricao.val(spanDescricao.html());
            inputTitulo.val(spanTitulo.html());

            spanDescricao.html(inputDescricao);
            spanTitulo.html(inputTitulo);

            postit.height('auto');
            btnEditar.hide();

            btnSalvar.on('click',function() {
                api.atualizarTarefa(
                    elementoTarefa.data('id'),
                    inputTitulo.val(),
                    inputDescricao.val(),
                    elementoTarefa
                );
            });

            btnSalvar.appendTo(postit);
            this.atualizarAlturas(true);
        }


    };


    $.fn.gerenciadorTarefas = function(opcoes) {

        //Atributos padrao de nossa configuraçao
        var configuracoes = $.extend({
            listagem: ".lista-tarefas",
            formulario: ".form-tarefas",
        }, opcoes );

        frontend.listagem = $(configuracoes.listagem);


        frontend.listagem.sortable({
            helper: "clone",

            placeholder: "tarefas",
            update:function(a,b) {
                frontend.atualizarPrioridades();
            }
        });

        frontend.listagem.disableSelection();

        frontend.formulario = $(configuracoes.formulario);
        frontend.formulario.on("submit", function() {
            return api.adicionar();
        });

        frontend.inicializado = true;
        api.listagem();

        $(window).on('resize', function() {
            frontend.atualizarAlturas(true);
        })


    }
}(jQuery));