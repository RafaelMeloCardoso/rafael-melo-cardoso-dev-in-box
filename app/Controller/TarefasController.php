<?php

/**
 * Author: Rafael Melo Cardoso <xinhus@gmail.com>
 * Date: 10/30/16
 * Time: 1:31 PM
 */

/**
 * Class TarefasController
 * Responsável por controlar os acessos a nossa API
 */
class TarefasController extends AppController
{
    public $uses = array('Tarefa');
    public $components = array('RequestHandler');

    /**
     * Realiza a listagem de todas as tarefas.
     */
    public function index()
    {
        $tarefas = $this->Tarefa->find('all', array(
            'order' => 'Tarefa.prioridade ASC'
        ));
        $this->set(array(
            'Tarefas'       => $tarefas,
            '_serialize'    => array('Tarefas')
        ));
    }

    /**
     * Método para controlar a adiçao de tarefas
     */
    public function add() {
        $tarefa = array();
        $sucesso = false;

        try {
            $tarefa = $this->Tarefa->inserirTarefa($this->request->data);
            $mensagem = 'Tarefa inserida com sucesso.';
            $tarefa = $tarefa['Tarefa'];
            $sucesso = true;
        } catch (Exception $e) {
            $mensagem = 'Erro ao inserir sua tarefa!';
            $mensagem .= "\n" . $e->getMessage();
        }

        $this->set(array(
            'mensagem'      => $mensagem,
            'Tarefa'        => $tarefa,
            'sucesso'       => $sucesso,
            '_serialize'    => array('mensagem', 'Tarefa', 'sucesso')
        ));
    }

    /**
     * Método responsável por controlar a visualização da tarefa específica
     * @param $id
     */
    public function view($id ) {

        $tarefa = $this->Tarefa->findById($id);

        $this->set(array(
            'Tarefa'        => isset($tarefa['Tarefa']) ? $tarefa['Tarefa'] : array() ,
            '_serialize'    => array('mensagem', 'Tarefa')
        ));
    }

    /**
     * Realiza a edição das tarefas
     * @param $id
     */
    public function edit($id) {
        $tarefa = array();
        $sucesso = false;

        try {
            $tarefa = $this->Tarefa->editarTarefa($id, $this->request->data);
            $mensagem = 'Tarefa atualizada com sucesso.';
            $tarefa = $tarefa['Tarefa'];
            $sucesso = true;
        } catch (Exception $e) {
            $mensagem = 'Erro ao atualizada sua tarefa!';
            $mensagem .= "\n" . $e->getMessage();
        }

        $this->set(array(
            'mensagem'      => $mensagem,
            'Tarefa'        => $tarefa,
            'sucesso'       => $sucesso,
            '_serialize'    => array('mensagem', 'Tarefa', 'sucesso')
        ));
    }


    /**
     * Remove uma tarefa.
     * PS: Em um projeto reconsideraria a ideia de excluir já que podemos apenas alterar uma flag.
     * @param $id
     */
    public function delete($id) {
        if ($this->Tarefa->delete($id)) {
            $mensagem = 'Tarefa removida com sucesso.';
        } else {
            $mensagem = 'Erro ao remover sua tarefa!';
        }
        $this->set(array(
            'mensagem' => $mensagem,
            '_serialize' => array('mensagem')
        ));
    }


}
