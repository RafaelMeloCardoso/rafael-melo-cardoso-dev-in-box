<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $this->fetch('title'); ?></title>
    <meta content="width=device-width,initial-scale=1" name=viewport>
    <meta http-equiv="Content-Language" content="pt-br">
    <base href="<?php echo Router::url('/', true) ?>">
    <?php
    /** @var HtmlHelper $helper */
    $helper = $this->Html;
    echo $helper->meta('icon');

    echo $helper->css('default');
    echo $helper->css('https://fonts.googleapis.com/icon?family=Material+Icons', array('inline' => false));

    echo $this->fetch('meta');
    echo $this->fetch('css');
    ?>
</head>
<body>
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>
    <?php echo $helper->script('default'); ?>
</body>
</html>
