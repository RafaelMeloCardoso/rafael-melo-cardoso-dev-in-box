<div class="container">
    <div class="row">
        <h1 class="page-title">Gerenciador de Tarefas</h1>
        <form class="form-horizontal form-tarefas">

            <div class="form-group">
                <label for="titulo" class="control-label">Título</label>
                <div class="form-input">
                    <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título" maxlength="50">
                </div>
            </div>
            <div class="form-group">
                <label for="descricao" class="control-label">Descrição</label>
                <div class="form-input">
                    <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição" maxlength="255">
                    <small>As novas tarefas vao ser inseridas com a maior prioridade.<BR/>Caso queira voce pode modificar sua posiçao</small>
                </div>
            </div>

            <div class="form-group">
                <div class="form-button">
                    <button type="submit" class="btn btn-success">Criar Tarefa</button>
                </div>
            </div>

        </form>
    </div>
    <div class="row">
        <ul class="lista-tarefas"></ul>
    </div>
</div>