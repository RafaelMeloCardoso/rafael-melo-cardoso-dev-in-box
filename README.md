# Gerenciador de Tarefas #
## Rafael Melo Cardoso - Dev In Box ##

### Questão 01 ###
A resposta da questão 1 se localiza no Branche questao_1

### Questão 02 ###
A resposta da questão 2 se localiza no Branche questao_2

### Questão 03 ###
A resposta da questão 3 se localiza no Branche questao_3


### Produção ###
Para colocar o Gerenciador de Tarefas em produção, após baixar o respositório você deve executar o script de instalação do banco (install/database.sql) em um banco de sua escolha.

Depois modificar as configurações de conexão com o banco em app/Config/database.php.


Pré Requisistos.

* Apache com o módulo mod_rewrite habilitado.
* php-xml instalado. (Caso queira utilizar a api em rest | o que não é recomendado.)
* Além da versão do php 5.3 ou superior.
* Você também deve dar permissões para as pastas app/tmp de escrita. 

### Api Rest ###
O endereço irá variar de onde você extraiu e alocou o projeto.
Todas os endereços são a partir da url base do seu projeto.

* Listagem  [GET] /tarefas.json 
* Adicionar [POST] /tarefas.json 
* Visualizar [GET] /tarefas/{id}.json 
* Editar [POST|PUT] /tarefas/{id}.json 
* Remover [DELETE] /tarefas/{id}.json 





### Desenvolvimento ###
Caso queira modificar o projeto:

Pré Requisistos.
* npm instalado.

Instale as dependências:

```
#!node

npm install

bower install
```

Caso você precise gerar um novo sass ou js. Utilize:


```
#!gulp

gulp
```
